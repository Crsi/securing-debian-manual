<?xml version="1.0"?>
<chapter><title>Before and during the installation</title>
<section id="bios-passwd"><title>Choose a BIOS password</title>
<para>
Before you install any operating system on your computer, set up a
BIOS password. After installation (once you have enabled bootup
from the hard disk) you should go back to the BIOS and change the 
boot sequence to disable booting from floppy, CD-ROM and other 
devices that shouldn't boot. Otherwise a cracker only needs 
physical access and a boot disk to access your entire system.</para>

<para>Disabling booting unless a password is supplied is even better. This can be 
very effective if you run a server, because it is not rebooted very often. The 
downside to this tactic is that rebooting requires human intervention which 
can cause problems if the machine is not easily accessible.</para>

<para>Note: many BIOSes have well known default master passwords, and
applications also exist to retrieve the passwords from the
BIOS. Corollary: don't depend on this measure to secure console access
to system.</para>
</section>

<section><title>Partitioning the system</title>

    <section><title>Choose an intelligent partition scheme</title>
<para>
An intelligent partition scheme depends on how the machine is
used. A good rule of thumb is to be fairly liberal with your
partitions and to pay attention to the following factors:</para>

<itemizedlist>
<listitem><para>Any directory tree which a user has write permissions to, such
as e.g. <filename>/home</filename>, <filename>/tmp</filename> and
<filename>/var/tmp/</filename>, should be on a separate partition. This
reduces the risk of a user DoS by filling up your "/" mount point and
rendering the system unusable (Note: this is not strictly true, since
there is always some space reserved for root which a normal user
cannot fill), and it also prevents hardlink attacks.
<footnote><para>
A very good example of this kind of attacks using /tmp is detailed in
<ulink url="http://www.hackinglinuxexposed.com/articles/20031111.html"
name="The mysteriously persistently exploitable program (contest)"/>
and
<ulink url="http://www.hackinglinuxexposed.com/articles/20031214.html"
name="The mysteriously persistently exploitable program explained"/>
(notice that the incident is Debian-related). It is basicly an attack
in which a local user <emphasis>stashes</emphasis> away a vulnerable setuid
application by making a hard link to it, effectively avoiding any
updates (or removal) of the binary itself made by the system
administrator. Dpkg was recently fixed to prevent this (see <ulink
url="http://bugs.debian.org/225692" name="225692" />) but other setuid
binaries (not controlled by the package manager) are at risk if
partitions are not setup correctly. 
</para></footnote></para></listitem>


<listitem><para>Any partition which can fluctuate, e.g. <filename>/var</filename>
(especially <filename>/var/log</filename>) should also be on a separate
partition. On a Debian system, you should create <filename>/var</filename> a
little bit bigger than on other systems, because downloaded packages (the apt
cache) are stored in <filename>/var/cache/apt/archives</filename>.</para></listitem> 

<listitem><para>Any partition where you want to install non-distribution
software should be on a separate partition. According to the File
Hierarchy Standard, this is <filename>/opt</filename> or <filename>/usr/local</filename>.
If these are separate partitions, they will not be erased if you 
(have to) reinstall Debian itself.</para></listitem>

<listitem><para>From a security point of view, it makes sense to try to move
static data to its own partition, and then mount that partition
read-only. Better yet, put the data on read-only media. See below for
more details.</para></listitem>
</itemizedlist>

<para>In the case of a mail server it is important to have a separate
partition for the mail spool. Remote users (either knowingly or
unknowingly) can fill the mail spool (<filename>/var/mail</filename> and/or
<filename>/var/spool/mail</filename>). If the spool is on a separate
partition, this situation will not render the system
unusable. Otherwise (if the spool directory is on the same
partition as <filename>/var</filename>) the system might have important
problems: log entries will not be created, packages cannot be
installed, and some programs might even have problems starting up (if
they use <filename>/var/run</filename>).</para>

<para>Also, for partitions in which you cannot be sure of the needed
space, installing Logical Volume Manager
(<application>lvm-common</application> and the needed binaries for your
kernel, this might be either <application>lvm10</application>,
<application>lvm6</application>, or <application>lvm5</application>). Using
<literal>lvm</literal>, you can create volume groups that expand multiple
physical volumes.</para>
</section>

<section><title>Selecting the appropriate file systems</title>
<para>During the system partitioning you also have to decide which file system you
want to use. The default file system<footnote><para>Since Debian GNU/Linux 4.0,
codename <literal>etch</literal></para></footnote> selected in the
Debian installation for Linux partitions is <literal>ext3</literal>, a journaling 
file system. It is recommended that you always use a journaling file system,
such as <literal>ext3</literal>, <literal>reiserfs</literal>, <literal>jfs</literal> or <literal>xfs</literal>, to
minimize the problems derived from a system crash in the following
cases:</para>

<itemizedlist>

<listitem><para>for laptops in all the file systems installed. That way if you
run out of battery unexpectedly or the system freezes due to a
hardware issue (such as X configuration which is somewhat common) you
will be less likely to lose data during a hardware reboot.</para></listitem>

<listitem><para>for production systems which store large amounts of data (like
mail servers, ftp servers, network file systems...) it is recommended
on these partitions. That way, in the event of a system crash, the
server will take less time to recover and check the file systems, and
data loss will be less likely.</para></listitem>

</itemizedlist>

<para>Leaving aside the performance issues regarding journalling
file systems (since this can sometimes turn into a religious war), it
is usually better to use the <literal>ext3</literal> file system. The reason for
this is that it is backwards compatible with <literal>ext2</literal>, so if
there are any issues with the journalling you can disable it and still
have a working file system. Also, if you need to recover the system
with a bootdisk (or CD-ROM) you do not need a custom kernel. If the
kernel is 2.4 or 2.6 <literal>ext3</literal> support is already available, if it
is a 2.2 kernel you will be able to boot the file system even if you
lose journalling capabilities. If you are using other journalling
file systems you will find that you might not be able to recover unless
you have a 2.4 or 2.6 kernel with the needed modules built-in.
If you are stuck with a 2.2 kernel on the rescue disk, it might be even
more difficult to have it access <literal>reiserfs</literal> or <literal>xfs</literal>.</para>

<para>In any case, data integrity might be better under <literal>ext3</literal> since it 
does file-data journalling while others do only meta-data journalling, see
<ulink url="http://lwn.net/2001/0802/a/ext3-modes.php3"/>.</para>

<para>Notice, however, that there are some partitions that might not
benefit from using a journaling filesystem. For example, if you are using a
separate partition for <filename>/tmp/</filename> you might be better off
using a standard <literal>ext2</literal> filesystem as it will be cleaned up
when the system boots.</para>
</section>
</section>

<section><title>Do not plug to the Internet until ready</title>

<para>The system should not be immediately connected to the Internet
during installation. This could sound stupid but network installation
is a common method. Since the system will install and activate
services immediately, if the system is connected to the Internet and
the services are not properly configured you are opening it to attack.</para>

<para>Also note that some services might have security
vulnerabilities not fixed in the packages you are using for
installation. This is usually true if you are installing from old
media (like CD-ROMs). In this case, the system could even be compromised
before you finish installation!</para>

<para>Since Debian installation and upgrades can be done over the
Internet you might think it is a good idea to use this feature on
installation. If the system is going to be directly connected to the
Internet (and not protected by a firewall or NAT), it is best to
install without connection to the Internet, using a local packages
mirror for both the Debian package sources and the security
updates. You can set up package mirrors by using another system
connected to the Internet with Debian-specific tools (if it's a Debian
system) like <application>apt-move</application> or
<application>apt-proxy</application>, or other common mirroring tools, to
provide the archive to the installed system. If you cannot do this,
you can set up firewall rules to limit access to the system while doing
the update (see <xref linkend="fw-security-update" />).</para>
</section>
<section><title>Set a root password</title>
<para>
Setting a good root password is the most basic requirement for having
a secure system. See 
<citerefentry><refentrytitle>passwd</refentrytitle><manvolnum>1</manvolnum></citerefentry>
for some hints
on how to create good passwords. You can also use an automatic
password generation program to do this for you (see <xref
linkend="user-pwgen" />).</para>

<para>
Plenty of information on choosing good passwords can be found on the
Internet; two that provide a decent summary and rationale are Eric Wolfram's 
<ulink name="How to: Pick a Safe Password"
url="http://wolfram.org/writing/howto/password.html"/> and 
Walter Belgers' 
<ulink name="Unix Password Security"
url="http://www.belgers.com/write/pwseceng.txt"/></para>
<!--Also at http://citeseer.ist.psu.edu/8481.html -->
</section>

<section><title>Run the minimum number of services required</title>

<para>Services are programs such as ftp servers and web servers. Since
they have to be <emphasis>listening</emphasis> for incoming connections that
request the service, external computers can connect to yours. Services
are sometimes vulnerable (i.e. can be compromised under a given
attack) and hence present a security risk.</para>

<para>You should not install services which are not needed on your
machine. Every installed service might introduce new, perhaps not
obvious (or known), security holes on your computer.</para>

<para>As you may already know, when you install a given service the
default behavior is to activate it. In a default Debian installation,
with no services installed, the number of running services is quite
low and the number of network-oriented services is even lower. In a
default Debian 3.1 standard installation you will end up with OpenSSH,
Exim (depending on how you configured it) and the RPC portmapper
available as network services<footnote><para>The footprint in Debian 3.0 and
earlier releases wasn't as tight, since some <command>inetd</command>
services were enabled by default. Also standard installations of
Debian 2.2 installed the NFS server as well as the telnet
server.</para></footnote>. If you did not go through a standard installation
but selected an expert installation you can end up with no active
network services. The RPC portmapper is installed by default because
it is needed for many services, for example NFS, to run on a given
system. However, it can be easily removed, see <xref linkend="rpc" /> for more
information on how to secure or disable RPC services.</para>

<para>When you install a new network-related service (daemon) in your
Debian GNU/Linux system it can be enabled in two ways: through the
<command>inetd</command> superdaemon (i.e. a line will be added to
<filename>/etc/inetd.conf</filename>) or through a standalone program that
binds itself to your network interfaces. Standalone programs are
controlled through the <filename>/etc/init.d</filename> files, which are
called at boot time through the SysV mechanism (or an alternative one)
by using symlinks in <filename>/etc/rc?.d/*</filename> (for more information
on how this is done read
<filename>/usr/share/doc/sysvinit/README.runlevels.gz</filename>).</para>

<para>If you want to keep some services but use them rarely, use the
<command>update-*</command> commands, e.g. <command>update-inetd</command> and
<command>update-rc.d</command> to remove them from the startup process. For
more information on how to disable network services read <xref
linkend="disableserv" />. If you want to change the default behaviour of
starting up services on installation of their associated
packages<footnote><para>This is desirable if you are setting up a
development chroot, for example.</para></footnote> use
<command>policy-rc.d</command>, please read
<filename>/usr/share/doc/sysv-rc/README.policy-rc.d.gz</filename> for more
information.</para>

<para><command>invoke-rc.d</command> support is mandatory in Debian, which means that
for Debian 4.0 <emphasis>etch</emphasis> and later releases you can write a policy-rc.d
file that forbids starting new daemons before you configure them. Although no
such scripts are packaged yet, they are quite simple to write. See
<application>policyrcd-script-zg2</application>.</para>

<section id="disableserv"><title>Disabling daemon services</title>

<para>Disabling a daemon service is quite simple. You either remove the
package providing the program for that service or you remove or rename
the startup links under <filename>/etc/rc${runlevel}.d/</filename>. If you
rename them make sure they do not begin with 'S' so that they don't
get started by <command>/etc/init.d/rc</command>. Do not remove all the
available links or the package management system will regenerate them
on package upgrades, make sure you leave at least one link (typically
a 'K', i.e. kill, link). For more information read
<ulink url="http://www.debian.org/doc/manuals/reference/ch-system.en.html#s-custombootscripts"
name="Customizing runlevels"/> section of the 
Debian Reference (Chapter 2 - Debian fundamentals).</para>

<para>You can remove these links manually or using <literal>update-rc.d</literal>
(see 
<citerefentry><refentrytitle>update-rc.d</refentrytitle><manvolnum>8</manvolnum></citerefentry>). 
For example, you can
disable a service from executing in the multi-user runlevels by doing:

<screen>
  # update-rc.d <varname>name</varname> stop <varname>XX</varname> 2 3 4 5 .
</screen></para>


<para>Where <emphasis>XX</emphasis> is a number that determines when the stop action
for that service will be executed. Please note that, if you are
<emphasis>not</emphasis> using <application>file-rc</application>, <literal>update-rc.d -f
<varname>service</varname> remove</literal> will not work properly, since
<emphasis>all</emphasis> links are removed, upon re-installation or upgrade of the
package these links will be re-generated (probably not what you
wanted). If you think this is not intuitive you are probably right
(see <ulink url="http://bugs.debian.org/67095" name="Bug 67095"/>). From
the manpage:
<screen>
  If any files /etc/rc<varname>runlevel</varname>.d/[SK]??name already exist then
  update-rc.d does nothing.  This is so that the system administrator 
  can rearrange the  links,  provided that  they  leave  at  least one
  link remaining, without having their configuration overwritten.
</screen></para>

<para>If you are using <application>file-rc</application> all the information
regarding services bootup is handled by a common configuration file
and is maintained even if packages are removed from the system.</para>

<para>You can use the TUI (Text User Interface) provided by
<application>sysv-rc-conf</application> to do all these changes easily
(<command>sysv-rc-conf</command> works both for <application>file-rc</application>
and normal System V runlevels). You will also find similar GUIs for 
desktop systems. You can also use the command line interface
of <application>sysv-rc-conf</application>:
<screen>
  # sysv-rc-conf foobar off
</screen></para>
 
<para>The advantage of using this utility is that 
the rc.d links are returned to the status they had before the 'off' call 
if you re-enable the service with:
<screen>
  # sysv-rc-conf foobar on
</screen></para>


<para>Other (less recommended) methods of disabling services are: </para>

<itemizedlist>

<listitem><para>Removing the  <filename>/etc/init.d/<varname>service_name</varname></filename> script
and removing the startup links using:

<screen>
  # update-rc.d <varname>name</varname> remove
</screen></para></listitem>

<listitem><para>Move the script file
(<filename>/etc/init.d/<varname>service_name</varname></filename>) to another name
(for example <filename>/etc/init.d/OFF.<varname>service_name</varname></filename>).
This will leave dangling symlinks under
<filename>/etc/rc${runlevel}.d/</filename> and will generate error messages
when booting up the system.</para></listitem>

<listitem><para>Remove the execute permission from the
<filename>/etc/init.d/<varname>service_name</varname></filename> file. That will
also generate error messages when booting.</para></listitem>

<listitem><para>Edit the <filename>/etc/init.d/<varname>service_name</varname></filename> script
to have it stop immediately once it is executed (by adding an
<command>exit 0</command> line at the beginning or commenting out the
<literal>start-stop-daemon</literal> part in it). If you do this, you will not be able to 
use the script to startup the service manually later on.</para></listitem>

</itemizedlist>

<para>Nevertheless, the files under <filename>/etc/init.d</filename> are
configuration files and should not get overwritten due to package
upgrades if you have made local changes to them.</para>

<para>Unlike other (UNIX) operating systems, services in
Debian cannot be disabled by modifying files in
<filename>/etc/default/<varname>service_name</varname></filename>.</para>

<para>FIXME: Add more information on handling daemons using
<application>file-rc</application>.</para>
</section>

<section id="inetd"><title>Disabling <command>inetd</command> or its services</title>
<para>
You should check if you really need the <command>inetd</command> daemon nowadays.
Inetd was always a way to compensate for kernel deficiencies, but those have
been taken care of in modern Linux kernels.
Denial of Service possibilities exist against <command>inetd</command> (which can
increase the machine's load tremendously), and many people always preferred
using stand-alone daemons instead of calling services via <command>inetd</command>.
If you still want to run some kind of <command>inetd</command> service, then at 
least switch to a more configurable Inet daemon like <command>xinetd</command>,
<command>rlinetd</command> or <command>openbsd-inetd</command>.</para>
<para>
You should stop all unneeded Inetd services on your system, like
<command>echo</command>, <command>chargen</command>, <command>discard</command>,
<command>daytime</command>, <command>time</command>, <command>talk</command>,
<command>ntalk</command> and r-services (<command>rsh</command>, <command>rlogin</command>
and <command>rcp</command>) which are
considered HIGHLY insecure (use <command>ssh</command> instead). </para>

<para>You can disable services by editing <filename>/etc/inetd.conf</filename> 
directly, but Debian provides a better alternative: <literal>update-inetd</literal> 
(which comments the services in a way that it can easily be turned on again). 
You could remove the <command>telnet</command> daemon by executing this commands to 
change the config file and to restart the daemon (in this case the 
<command>telnet</command> service is disabled):

<screen>
  /usr/sbin/update-inetd --disable telnet
</screen></para>
<!-- # /etc/init.d/inetd restart Not needed since the manpage says update-inetd
sends a SIGHUP, commented out as suggested by Dariusz Puchalak -->

<para>If you do want services listening, but do not want to have them listen on 
all IP addresses of your host, you might want to use an undocumented feature 
on <command>inetd</command> (replace service name with service@ip syntax) or use
an alternative <command>inetd</command> daemon like <command>xinetd</command>.</para>
</section>
</section>

<section><title>Install the minimum amount of software required</title>

<para>Debian comes with <emphasis>a lot</emphasis> of software, for example the
Debian 3.0 <emphasis>woody</emphasis> release includes 6 or 7 (depending on
architecture) CD-ROMs of software and thousands of packages,
and the Debian 3.1 <emphasis>sarge</emphasis> release ships with around 13 CD-ROMs
of software. With so much software, and even if
the base system installation is quite reduced
<footnote><para>For example, in Debian woody it is around 400-500 Mbs, try this:
<screen>
  $ size=0
  $ for i in `grep -A 1 -B 1 "^Section: base" /var/lib/dpkg/available |
  grep -A 2 "^Priority: required" |grep "^Installed-Size" |cut -d : -f 2
  `; do size=$(($size+$i)); done
  $ echo $size
  47762
</screen>
</para></footnote>
you might get carried away and install more than is really needed
for your system.</para>

<para>Since you already know what the system is for (don't you?) you
should only install software that is really needed for it to work. Any
unnecessary tool that is installed might be used by a user that wants
to compromise the system or by an external intruder that has gotten
shell access (or remote code execution through an exploitable
service).</para>

<para>The presence, for example, of development utilities (a C compiler) or 
interpreted languages (such as <command>perl</command> - but see below -, 
<command>python</command>, <command>tcl</command>...) may help an attacker compromise the 
system even further:</para>

<itemizedlist>
<listitem><para>allowing him to do privilege escalation. It's easier, for
example, to run local exploits in the system if there is a debugger
and compiler ready to compile and test them!</para></listitem>

<listitem><para>providing tools that could help the attacker to use the
compromised system as a <emphasis>base of attack</emphasis> against other systems.
<footnote><para>
Many intrusions are made just to get access to resources to do
illegitimate activity (denial of service attacks, spam, rogue ftp
servers, dns pollution...) rather than to obtain confidential
data from the compromised system.
</para></footnote></para></listitem>
</itemizedlist>

<para>Of course, an intruder with local shell access can download his own
set of tools and execute them, and even the shell itself can be used
to make complex programs. Removing unnecessary software will not help
<emphasis>prevent</emphasis> the problem but will make it slightly more difficult
for an attacker to proceed (and some might give up in this situation
looking for easier targets). So, if you leave tools in a production
system that could be used to remotely attack systems (see <xref
linkend="vuln-asses" />) you can expect an intruder to use them too if
available.</para>

<para>Please notice that a default installation of Debian <emphasis>sarge</emphasis>
(i.e. an installation where no individual packages are selected) will
install a number of development packages that are not usually needed.
This is because some development packages are of <emphasis>Standard</emphasis> priority.
If you are not going to do any development you can safely remove the
following packages from your system, which will also help free up some
space:

<screen>
Package                    Size
------------------------+--------
gdb                     2,766,822
gcc-3.3                 1,570,284
dpkg-dev                  166,800
libc6-dev               2,531,564
cpp-3.3                 1,391,346
manpages-dev            1,081,408
flex                      257,678
g++                         1,384 (Note: virtual package)
linux-kernel-headers    1,377,022
bin86                      82,090
cpp                        29,446
gcc                         4,896 (Note: virtual package)
g++-3.3                 1,778,880
bison                     702,830
make                      366,138
libstdc++5-3.3-dev        774,982
</screen></para>

<para>This is something that is fixed in releases post-sarge,
see <ulink url="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=301273"
name="Bug #301273"/> 
and <ulink url="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=301138"
name="Bug #301138"/>. Due to a bug in the installation system this did not
happen when installing with the installation system of the Debian 3.0
<emphasis>woody</emphasis> release.</para>

<section><title>Removing Perl</title>
<para>You must take into account that removing <command>perl</command> might not be too 
easy (as a matter of fact it can be quite difficult) in a Debian system since 
it is used by many system utilities. Also, the <application>perl-base</application> is 
<emphasis>Priority: required</emphasis> (that about says it all). It's still doable, but 
you will not be able to run any <command>perl</command> application in the system; 
you will also have to fool the package management system to think that the
<application>perl-base</application> is installed even if it's not. <footnote><para>You can 
make (on another system) a dummy package with <application>equivs</application>.
</para></footnote></para>

<para>Which utilities use <command>perl</command>? You can see for yourself:

<screen>
  $ for i in /bin/* /sbin/* /usr/bin/* /usr/sbin/*; do [ -f $i ] &amp;&amp; {
  type=`file $i | grep -il perl`; [ -n "$type" ] &amp;&amp; echo $i; }; done
</screen></para>

<para>These include the following utilities in packages with priority
<emphasis>required</emphasis> or <emphasis>important</emphasis>:</para>

<itemizedlist>
<listitem><para><filename>/usr/bin/chkdupexe</filename> of package
<application>util-linux</application>.</para></listitem>

<listitem><para><filename>/usr/bin/replay</filename> of package
<application>bsdutils</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/cleanup-info</filename> of package
<application>dpkg</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/dpkg-divert</filename> of package
<application>dpkg</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/dpkg-statoverride</filename> of package
<application>dpkg</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/install-info</filename> of package
<application>dpkg</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/update-alternatives</filename> of package
<application>dpkg</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/update-rc.d</filename> of package
<application>sysvinit</application>.</para></listitem>

<listitem><para><filename>/usr/bin/grog</filename> of package
<application>groff-base</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/adduser</filename> of package
<application>adduser</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/debconf-show</filename> of package
<application>debconf</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/deluser</filename> of package
<application>adduser</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/dpkg-preconfigure</filename> of package
<application>debconf</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/dpkg-reconfigure</filename> of package
<application>debconf</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/exigrep</filename> of package
<application>exim</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/eximconfig</filename> of package
<application>exim</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/eximstats</filename> of package
<application>exim</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/exim-upgrade-to-r3</filename> of package
<application>exim</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/exiqsumm</filename> of package
<application>exim</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/keytab-lilo</filename> of package
<application>lilo</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/liloconfig</filename> of package
<application>lilo</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/lilo_find_mbr</filename> of package
<application>lilo</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/syslogd-listfiles</filename> of package
<application>sysklogd</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/syslog-facility</filename> of package
<application>sysklogd</application>.</para></listitem>

<listitem><para><filename>/usr/sbin/update-inetd</filename> of package
<application>netbase</application>.</para></listitem>

</itemizedlist>

<para>So, without Perl and, unless you remake these utilities in shell
script, you will probably not be able to manage any packages (so you
will not be able to upgrade the system, which is <emphasis>not a Good
Thing</emphasis>).</para>

<para>If you are determined to remove Perl from the Debian base system,
and you have spare time, submit bug reports to the previous packages
including (as a patch) replacements for the utilities above written in
shell script.</para>

<para>If you wish to check out which Debian packages depend on Perl you can use

<screen>
$ grep-available -s Package,Priority -F Depends perl
</screen></para>

<para>or
<screen>
$ apt-cache rdepends perl
</screen></para>
</section></section>

<section><title>Read the Debian security mailing lists</title>

<para>It is never wrong to take a look at either the debian-security-announce 
mailing list, where advisories and fixes to released packages are announced by 
the Debian security team, or at 
<ulink url="mailto:debian-security@lists.debian.org"/>, where you can participate 
in discussions about things related to Debian security.</para>

<para>In order to receive important security update alerts, send an email
to <ulink name="debian-security-announce-request@lists.debian.org"
url="mailto:debian-security-announce-request@lists.debian.org"/> with
the word "subscribe" in the subject line. You can also subscribe to
this moderated email list via the web page at
<ulink name="http://www.debian.org/MailingLists/subscribe"
url="http://www.debian.org/MailingLists/subscribe"/>.</para>

<para>This mailing list has very low volume, and by subscribing to it you
will be immediately alerted of security updates for the Debian
distribution. This allows you to quickly download new packages with
security bug fixes, which is very important in maintaining a secure
system (see <xref linkend="security-update"/> for details on how to do this).</para>
</section>
</chapter>
